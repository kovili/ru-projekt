import shutil
import sys
from PIL import Image
import os
import time
import random
from setuptools.msvc import winreg
from sklearn.preprocessing import StandardScaler
from feature_extraction import *
from preprocessing import *
from configuration_runner import ConfRunner
from image_marker import *
import video_utils as vu
import multiprocessing as mp


def print_hi(name):
    print(f'Hi, {name}')


def standardize(learn_vectors, n_distances, n_features, scaler=None):
    """
    Vraća standardizirane 2D matrice haralickovih značajki za sve slike.
    Standardizacija se provodi na sljedeći način:
    1. Svaka slika rastavi se na prozore. Prozor je vektor sa n_distances * n_features
       značajki. Takvi prozori se vertikalno poredaju tako da jedan stupac predstavlja jednu značajku.
    2. Značajke se skaliraju.
    3. Dobivena struktura se transformira u početni oblik.
    :param learn_vectors: 2D matrica haralickovih značajki za sve slike
    :param n_distances: broj udaljenosti
    :param n_features: broj značajki
    :param scaler: prethodno naučeni scaler (ako takav postoji)
    :return: standardizirane 2D matrice haralickovih značajki za sve slike
    """
    n_images = learn_vectors.shape[0]
    learn_windows = learn_vectors.reshape(-1, n_distances * n_features)
    if scaler is None:
        scaler = StandardScaler()
        scaler.fit(learn_windows)
    standardized_learn_windows = scaler.transform(learn_windows)
    standardized_learn_vectors = standardized_learn_windows.reshape(n_images, -1)
    return standardized_learn_vectors, scaler


def learn(conf_runner: ConfRunner):
    database_path = conf_runner.get_param("database_path")
    # read from database if one exists
    if database_path is not None:
        os.path.abspath(database_path)
        if os.path.exists(database_path):
            array = np.load(database_path)
            return array

    resize_dimension, window_size = conf_runner.get_param("resize_dim"), conf_runner.get_param("window_size")
    grey_levels, distances = conf_runner.get_param("grey_levels"), conf_runner.get_param("distances")
    directory_path = conf_runner.get_param("train_folder")
    learn_vector = []
    for image_path in os.listdir(directory_path):
        full_path = os.path.join(directory_path, image_path)
        image = Image.open(full_path)
        resized_image = resize(image, dimension=resize_dimension)
        greyscaled_image = grayscale(resized_image, levels=grey_levels)
        window = get_first_window(greyscaled_image, window_size)
        out_vector = map_window_to_vector(window, conf_runner)
        learn_vector.append(out_vector)
    in_matrix = np.array(learn_vector)
    # save to database if necessary
    if database_path is not None:
        np.save(database_path, in_matrix)
    return in_matrix


def classify(window_vector, vector_db, euclidean_dist, window_count, conf_runner: ConfRunner):
    neighbour_count, classification_limit = conf_runner.get_param("neighbour_count"), conf_runner.get_param(
        "classification_limit")
    classify_sum = 0
    classification = np.ndarray(dtype=np.uint8, shape=(window_count * window_count,))
    for row in range(window_count):
        for column in range(window_count):
            current_window = window_vector[row * window_count + column]
            classified_count = 0
            for vector in vector_db:
                vector_dist = np.linalg.norm(current_window - vector)
                if vector_dist < euclidean_dist:
                    classified_count += 1
                    if classified_count == neighbour_count:
                        classify_sum += 1
                        classification[row * window_count + column] = 1
                        break

            if not classified_count == neighbour_count:
                classification[row * window_count + column] = 0

    modified_image = mark_crowd_area(None if classify_sum < classification_limit else classification, window_count,
                                     conf_runner.get_param("original"))

    return classification, classify_sum, modified_image


def predict(directory_path, conf_runner: ConfRunner):
    resize_dimension, window_size = conf_runner.get_param("resize_dim"), conf_runner.get_param("window_size")
    grey_levels, distances = conf_runner.get_param("grey_levels"), conf_runner.get_param("distances")
    euclidean_dist, window_count = conf_runner.get_param("euclidean_dist"), conf_runner.get_param("window_count")
    vector_db, scaler = conf_runner.get_param("vector_db"), conf_runner.get_param("scaler")
    classification_limit = conf_runner.get_param("classification_limit")
    classifications = []
    # init classifications directories
    init_classification_directories(conf_runner)
    # retrieve and permute image list
    image_names = os.listdir(directory_path)
    random.shuffle(image_names)
    for image_path in image_names:
        window_vector = []
        full_path = os.path.join(directory_path, image_path)
        image = Image.open(full_path)
        conf_runner.add_param("original", image)
        resized_image = resize(image, dimension=resize_dimension)
        greyscaled_image = grayscale(resized_image, levels=grey_levels)
        windows = get_windows(greyscaled_image, window_size, window_size // 2)
        for window in windows:
            out_vector = map_window_to_vector(window, conf_runner)
            window_vector.append(out_vector)
        window_vector = scaler.transform(window_vector)
        classification, classified_count, modified_image = classify(window_vector, vector_db, euclidean_dist,
                                                                    window_count, conf_runner)
        store_classified_image(image, image_path, classified_count < classification_limit, conf_runner)
        classifications.append(classification)
    return classifications


def init_classification_directories(conf_runner):
    pos_dir = os.path.abspath(conf_runner.get_param("positive_directory"))
    neg_dir = os.path.abspath(conf_runner.get_param("negative_directory"))
    init_classification_directory(pos_dir)
    init_classification_directory(neg_dir)


def init_classification_directory(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    else:
        for filename in os.listdir(dir):
            file_path = os.path.join(dir, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))


def store_classified_image(image, image_path, classification, conf_runner):
    pos_dir = os.path.abspath(conf_runner.get_param("positive_directory"))
    neg_dir = os.path.abspath(conf_runner.get_param("negative_directory"))

    full_path = os.path.join(neg_dir if classification else pos_dir, image_path)
    print(f"Klasifikacija za sliku {image_path} je {'negativna' if classification else 'pozitivna'}")
    image.save(full_path)


def map_window_to_vector(window, conf_runner):
    # extract parameters
    haralick_features = conf_runner.get_param('haralick_features')
    distances = conf_runner.get_param('distances')
    n_distances = len(distances)
    angles = conf_runner.get_param('angles')
    grey_levels = conf_runner.get_param('grey_levels')

    out_vector = np.zeros(shape=(conf_runner.get_param("vector_size")))
    # calculate features
    glcm = calculate_glc_matrix(window, distances, angles, levels=grey_levels)
    calculated_hf = calculate_haralick_features(glcm, haralick_features)
    # write features to output vector
    for i, c_hf in enumerate(calculated_hf):
        out_vector[i * n_distances:(i + 1) * n_distances] = calculated_hf[i][:]
    return out_vector


def predict_single_image(image, conf_runner):
    resize_dimension, window_size = conf_runner.get_param("resize_dim"), conf_runner.get_param("window_size")
    grey_levels, distances = conf_runner.get_param("grey_levels"), conf_runner.get_param("distances")
    euclidean_dist, window_count = conf_runner.get_param("euclidean_dist"), conf_runner.get_param("window_count")
    vector_db, scaler = conf_runner.get_param("vector_db"), conf_runner.get_param("scaler")
    window_vector = []
    conf_runner.add_param("original", image)
    resized_image = resize(image, dimension=resize_dimension)
    greyscaled_image = grayscale(resized_image, levels=grey_levels)
    windows = get_windows(greyscaled_image, window_size, window_size // 2)
    for window in windows:
        out_vector = map_window_to_vector(window, conf_runner)
        window_vector.append(out_vector)
    window_vector = scaler.transform(window_vector)
    classification, classified_count, modified_image = classify(window_vector, vector_db, euclidean_dist,
                                                                window_count, conf_runner)
    return modified_image


if __name__ == '__main__':
    try:
        conf_runner = ConfRunner()
        conf_runner.parse_configuration_file("config")

        if conf_runner.get_param("source") is None:
            print("Molim Vas upišite ime datoteke sa slikama koje želite klasificirati (atribut source u config datoteci).")
            raise EOFError

        input_data = learn(conf_runner)
        standardized_input_data, scaler = standardize(input_data, len(conf_runner.get_param("distances")),
                                                      len(conf_runner.get_param("haralick_features")))
        conf_runner.add_param("scaler", scaler)
        conf_runner.add_param("vector_db", standardized_input_data)

        classification = predict(conf_runner.get_param("source"), conf_runner)

        video_url = conf_runner.get_param("video_url")
        if video_url is not None:
            video_output_path = conf_runner.get_param("video_output_path")
            if video_output_path is not None:
                n_frames = vu.get_number_of_frames(video_url)
                num_processes = mp.cpu_count()
                frame_jump_unit = n_frames // num_processes  #
                vu.process_video(num_processes, video_url, n_frames, frame_jump_unit, conf_runner, video_output_path)
            else:
                print('Potrebno je zadati izlaznu datoteku.')  # nije bas verbose
    except:
        print("Nešto je pošlo po krivu! Molim Vas provjerite konfiguracijsku datoteku i probajte opet.")

