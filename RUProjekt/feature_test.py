import cProfile, pstats
import numpy as np
from PIL import Image
from preprocessing import grayscale, resize
from random import randint
from skimage.feature import greycomatrix, greycoprops
from feature_extraction import calculate_haralick_features, calculate_glc_matrix


def calculate_haralick_features_test(glc_matrix):
    pass


def calculate_glc_matrix_test(bytemap, distances=(1, 2, 3, 4, 5, 6), angles=(0, 45, 90, 135), levels=16):
    pass


if __name__ == '__main__':
    n = 64
    levels = 16
    bytemap = np.array([randint(0, levels - 1) for i in range(n ** 2)]).reshape(n, n)
    iterations = 1000
    pr = cProfile.Profile()
    pr.enable()
    for i in range(1000):
        glc_matrix = calculate_glc_matrix(bytemap)
        calculate_haralick_features(glc_matrix)
    pr.disable()
    pr.print_stats(sort='time')
