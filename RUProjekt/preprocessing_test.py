from PIL import Image, ImageSequence
import numpy as np
import time


def resize_test(imageSource: Image.Image, dimension):
	return imageSource.resize(dimension)


def to_byte_map_test():
	pass


def grayscale_test(PILImage: Image.Image):
	grayscale_image = PILImage.convert('L')
	return np.asarray(grayscale_image)


def get_frames_list(PILImage: Image.Image):
	return list(ImageSequence.Iterator(PILImage))


if __name__ == '__main__':
	picPath = "./test_images/440x440_test.jpg"
	iterations = 1000

	print("Vrijeme izvodenja steupa: " + str(time.perf_counter()))
	for i in range(0, iterations):
		img = Image.open(picPath)
		grayscale_test(img)
	print("Vrijeme izovdenja greyscale_test izvedeno " + str(iterations) + " puta: " + str(time.perf_counter()))

	for i in range(0, iterations):
		img = Image.open(picPath)
		resize_test(img, (100, 100))
	print("Vrijeme izovdenja resize_test izvedeno " + str(iterations) + " puta: " + str(time.perf_counter()))


	image_sequence_path = "./test_image_sequence/sample.gif"
	image_sequence = Image.open(image_sequence_path)
	sequence_frames = get_frames_list(image_sequence)
	print("\nLista slika")
	print(sequence_frames)