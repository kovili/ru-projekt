from moviepy.video.compositing.concatenate import concatenate_videoclips
from moviepy.video.io.VideoFileClip import VideoFileClip
from feature_extraction import *
from preprocessing import *
from configuration_runner import ConfRunner
from image_marker import *
import cv2
from os import remove
import multiprocessing as mp
from main import standardize, learn, classify, predict, map_window_to_vector, mark_crowd_area


def print_hi(name):
    print(f'Hi, {name}')


def merge_output_videos(num_processes):
    [print(process_number) for process_number in range(num_processes)]
    videos = [VideoFileClip("output_video{}.mp4".format(process_number)) for process_number in range(num_processes)]
    output_video = concatenate_videoclips(videos)
    output_video.write_videofile("output.mp4")
    [remove("output_video{}.mp4".format(process_number)) for process_number in range(num_processes)]


def process_video(n_processes):
    pool = mp.Pool(n_processes)
    pool.map(process_video_frames, range(n_processes))
    pool.close()
    pool.join()
    merge_output_videos(num_processes)


def get_number_of_frames(video_url):
    video_capture = cv2.VideoCapture(video_url)
    n_frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
    return n_frames


def process_video_frames(process_number):
    global video_url, n_frames, frame_jump_unit, gray_levels, conf_runner
    video_capture = cv2.VideoCapture(video_url)
    video_capture.set(cv2.CAP_PROP_POS_FRAMES, frame_jump_unit * process_number)
    frame_counter = 0
    width, height = (
        int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    )
    fps = int(video_capture.get(cv2.CAP_PROP_FPS))
    out = cv2.VideoWriter()
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    out.open("output_video{}.mp4".format(process_number), fourcc, fps, (width, height), True)
    while frame_counter < frame_jump_unit:
        ret, frame = video_capture.read()
        if frame is not None:
            frame_counter += 1
            pil_image = Image.fromarray(frame)
            conf_runner.add_param("original", pil_image)
            pil_image = predict_single_image(pil_image)
            opencv_image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
            out.write(opencv_image)
            if cv2.waitKey(22) & 0xFF == ord('q'):
                break
        else:
            break
    video_capture.release()
    cv2.destroyAllWindows()


def predict_single_image(image):
    resize_dimension, window_size = conf_runner.get_param("resize_dim"), conf_runner.get_param("window_size")
    grey_levels, distances = conf_runner.get_param("grey_levels"), conf_runner.get_param("distances")
    euclidean_dist, window_count = conf_runner.get_param("euclidean_dist"), conf_runner.get_param("window_count")
    vector_db, scaler = conf_runner.get_param("vector_db"), conf_runner.get_param("scaler")
    window_vector = []
    conf_runner.add_param("original", image)
    resized_image = resize(image, dimension=resize_dimension)
    greyscaled_image = grayscale(resized_image, levels=grey_levels)
    windows = get_windows(greyscaled_image, window_size, window_size // 2)
    for window in windows:
        out_vector = map_window_to_vector(window, conf_runner)
        window_vector.append(out_vector)
    window_vector = scaler.transform(window_vector)
    classification, classified_count, modified_image = classify(window_vector, vector_db, euclidean_dist,
                                                                window_count, conf_runner)
    return modified_image


video_url = "test_videos/Crowd.mp4"
frame_count = get_video_details(video_url)
num_processes = mp.cpu_count()
frame_jump_unit = frame_count // num_processes
conf_runner = ConfRunner()
conf_runner.parse_configuration_file("config")
input_data = learn(conf_runner)
standardized_input_data, scaler = standardize(input_data, len(conf_runner.get_param("distances")),
                                              len(conf_runner.get_param("haralick_features")))
conf_runner.add_param("scaler", scaler)
conf_runner.add_param("vector_db", standardized_input_data)

if __name__ == '__main__':
    print(standardized_input_data)
    process_video(mp.cpu_count())
