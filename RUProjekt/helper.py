import numpy as np
import cv2
from PIL import Image
from preprocessing import grayscale
import matplotlib.pyplot as plt


def get_frames(video_url, n_images=10, preview=False):
    """
    Vraća video kao niz slika.
    :param video_url: put do mp4 datoteke
    :param n_images: željeni broj slika
    :param preview: omogući prikaz videa
    :return:
    """
    video_capture = cv2.VideoCapture(video_url)
    frame_counter = 0
    images = []
    while frame_counter < n_images:
        ret, frame = video_capture.read()
        if frame is not None:
            frame_counter += 1
            image = Image.fromarray(frame)
            images.append(image)
            if preview:
                cv2.imshow('frame', frame)
            if cv2.waitKey(22) & 0xFF == ord('q'):
                break
        else:
            break
    video_capture.release()
    cv2.destroyAllWindows()
    return images


if __name__ == '__main__':
    # moze bilo kakav link na mp4
    video_url = 'test_videos\\Crowd.mp4'
    images = get_frames(video_url=video_url, n_images=10, preview=True)
    grayscale_image = grayscale(images[0], levels=16)
    plt.imshow(grayscale_image, cmap='gray')
    plt.show()
