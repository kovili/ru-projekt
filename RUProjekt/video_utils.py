from moviepy.video.compositing.concatenate import concatenate_videoclips
from moviepy.video.io.VideoFileClip import VideoFileClip
from feature_extraction import *
from main import predict_single_image
from preprocessing import *
from image_marker import *
import cv2
from os import remove
import multiprocessing as mp
import os


def merge_output_videos(num_processes, output_path):
    videos = [VideoFileClip("output_video_fragment{}.mp4".format(process_number)) for process_number in
              range(num_processes)]
    output_video = concatenate_videoclips(videos)
    output_video.write_videofile(output_path)
    [remove("output_video_fragment{}.mp4".format(process_number)) for process_number in range(num_processes)]


def get_number_of_frames(video_url):
    video_capture = cv2.VideoCapture(video_url)
    n_frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
    return n_frames


def process_video_frames(process_number, video_url, n_frames, frame_jump_unit, conf_runner):
    video_capture = cv2.VideoCapture(video_url)
    video_capture.set(cv2.CAP_PROP_POS_FRAMES, frame_jump_unit * process_number)
    frame_counter = 0
    width, height = (
        int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    )
    fps = int(video_capture.get(cv2.CAP_PROP_FPS))
    out = cv2.VideoWriter()
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    out.open("output_video_fragment{}.mp4".format(process_number), fourcc, fps, (width, height), True)
    while frame_counter < frame_jump_unit:
        ret, frame = video_capture.read()
        if frame is not None:
            frame_counter += 1
            pil_image = Image.fromarray(frame)
            conf_runner.add_param("original", pil_image)
            pil_image = predict_single_image(pil_image, conf_runner)
            opencv_image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
            out.write(opencv_image)
            if cv2.waitKey(22) & 0xFF == ord('q'):
                break
        else:
            break
    video_capture.release()
    cv2.destroyAllWindows()


def process_video(n_processes, video_url, n_frames, frame_jump_unit, conf_runner, output_path):
    pool = mp.Pool(n_processes)
    iterable = [(i, video_url, n_frames, frame_jump_unit, conf_runner) for i in range(n_processes)]
    pool.starmap(process_video_frames, iterable)
    pool.close()
    pool.join()
    merge_output_videos(n_processes, output_path)
