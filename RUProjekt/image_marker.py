from PIL import Image
import numpy as np

RED_COLOR_DELTA = 60
GREEN_COLOR_DELTA = -30
BLUE_COLOR_DELTA = -30

ABOVE_LEFT = 0
ABOVE = 1
ABOVE_RIGHT = 2
LEFT = 3

UL_START = "upLeftStart"
UL_END = "upLeftEnd"
UR_START = "upRightStart"
UR_END = "upRightEnd"
DL_START = "downLeftStart"
DL_END = "downLeftEnd"
DR_START = "downRightStart"
DR_END = "downRightEnd"


def mark_crowd_area(classification_array, windows_per_row_and_column, sourceImage: Image.Image):
    """
	-----METODA ZA SAD MODIFICIRA PREDANI OBJEKT SLIKE------

	Metoda oznacuje podrucja koja su oznacena kao mnostvo ljudi prema prednoj listi classification_array.
	Dimenzije jednog podrucja odredne su preko parametra windows_per_row_and_column te s obizirom na sirinu i visinu slike predan parametrom sourceImage.
	Metoda na kraju prikazuje modficiranu sliku.
	Podrucjima koja us oznacena kao mnostvo ljudi ce biti promijenjena

	:param classification_array: Lista koja predstavlja oznake klasifikacije predane slike. Duljina liste mora biti kvadrat parametra windows_per_row_and_column
	:param windows_per_row_and_column: Broj prozora u jendom retku/stupcu
	:param sourceImage: Izvorna slika
	:return:
	"""
    if classification_array is None:
        return sourceImage
    class_array = np.reshape(classification_array, (windows_per_row_and_column, windows_per_row_and_column))

    image_width = sourceImage.width
    image_height = sourceImage.height

    step_horizontal = image_width / (windows_per_row_and_column + 1)  # t1
    step_vertical = image_height / (windows_per_row_and_column + 1)  # t2

    window_width = step_horizontal * 2  # d1
    window_height = step_vertical * 2  # d2

    image_pixel_map = sourceImage.load()

    for i in range(class_array.shape[0]):
        for j in range(class_array.shape[1]):
            classification = class_array[i][j]
            if classification == 1:
                overlapDict = {}
                if j > 0:
                    overlapDict[LEFT] = class_array[i][j - 1]  # jel prozor lijevo od ovog oznacen
                else:
                    overlapDict[LEFT] = 0
                if i > 0:
                    overlapDict[ABOVE] = class_array[i - 1][j]  # jel prozor iznad ovog oznecen
                else:
                    overlapDict[ABOVE] = 0
                if i > 0 and j > 0:
                    overlapDict[ABOVE_LEFT] = class_array[i - 1][j - 1]  # jel prozor lijevo iznad ovog oznacen
                else:
                    overlapDict[ABOVE_LEFT] = 0
                if j < windows_per_row_and_column - 1 and i > 0:
                    overlapDict[ABOVE_RIGHT] = class_array[i - 1][j + 1]  # jel prozor desno iznad ovog oznacen
                else:
                    overlapDict[ABOVE_RIGHT] = 0

                start_horizontal = window_width / 2 * j
                end_horizontal = start_horizontal + window_width

                start_vertical = window_height / 2 * i
                end_vertical = start_vertical + window_height

                quadrant_dict = get_quadrant_dict(start_horizontal, end_horizontal, start_vertical, end_vertical)

                if overlapDict[LEFT] != 1 and overlapDict[ABOVE] != 1 and overlapDict[
                    ABOVE_LEFT] != 1:  # ako se ne preklapa, obojaj gornje lijevi kvadrant
                    paint_pixels(image_pixel_map, quadrant_dict[UL_START][0], quadrant_dict[UL_END][0],
                                 quadrant_dict[UL_START][1], quadrant_dict[UL_END][1], image_width, image_height)

                if overlapDict[ABOVE] != 1 and overlapDict[
                    ABOVE_RIGHT] != 1:  # ako se ne preklapa, obojaj gornje desni kvadrant trenutacnog prozora
                    paint_pixels(image_pixel_map, quadrant_dict[UR_START][0], quadrant_dict[UR_END][0],
                                 quadrant_dict[UR_START][1], quadrant_dict[UR_END][1], image_width, image_height)

                if overlapDict[LEFT] != 1:  # ako se ne preklapa, obojaj lijevi donji kvadrant trenutacnog prozora
                    paint_pixels(image_pixel_map, quadrant_dict[DL_START][0], quadrant_dict[DL_END][0],
                                 quadrant_dict[DL_START][1], quadrant_dict[DL_END][1], image_width, image_height)

                # donji desni se uvijek boja
                paint_pixels(image_pixel_map, quadrant_dict[DR_START][0], quadrant_dict[DR_END][0],
                             quadrant_dict[DR_START][1], quadrant_dict[DR_END][1], image_width, image_height)

    return sourceImage


def paint_pixels(pixel_map, start_x, end_x, start_y, end_y, image_width, image_height):
    """
	MODIFICIRA PREDANU MAPU A TIME I OBJEKT SLIKE KOJEM TA MAPA PRIPADA

	Mijenja mapu pixlea u predanom rasponu prema parametrima modula RED_COLOR_DELTA, GREEN_COLOR_DELTA, BLUE_COLOR_DELTA


	:param pixel_map: Mapa pixlea dobivena iz metode PIL.Image.Image.load()
	:param start_x: Pocenta horizontalna vrijednost
	:param end_x: Krajnja horizontalna vrijednost
	:param start_y: Pocetna vertikalna vrijednost
	:param end_y: Krajnja vertikalna vrijednost
	:param image_height:
	:param image_width:
	:return:
	"""
    start_x = int(start_x)
    start_y = int(start_y)
    end_x = int(end_x)
    end_y = int(end_y)

    end_x = min(end_x, image_width - 1)
    end_y = min(end_y, image_height - 1)
    for x in range(start_x, end_x):
        for y in range(start_y, end_y):
            r_pixel = min(255, pixel_map[x, y][0] + RED_COLOR_DELTA)
            g_pixel = max(0, pixel_map[x, y][1] + GREEN_COLOR_DELTA)
            b_pixel = max(0, pixel_map[x, y][2] + BLUE_COLOR_DELTA)
            pixel_map[x, y] = (r_pixel, g_pixel, b_pixel)


def get_quadrant_dict(start_x, end_x, start_y, end_y):
    quadrantDict = {}
    window_width = end_x - start_x
    window_height = end_y - start_y

    quadrantDict[UL_START] = (start_x, start_y)
    quadrantDict[UL_END] = (end_x - window_width / 2, end_y - window_height / 2)

    quadrantDict[UR_START] = (start_x + window_width / 2, start_y)
    quadrantDict[UR_END] = (end_x, end_y - window_height / 2)

    quadrantDict[DL_START] = (start_x, start_y + window_height / 2)
    quadrantDict[DL_END] = (end_x - window_width / 2, end_y)

    quadrantDict[DR_START] = (end_x - window_width / 2, end_y - window_height / 2)
    quadrantDict[DR_END] = (end_x, end_y)

    return quadrantDict
