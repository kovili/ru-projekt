import numpy as np
from PIL import Image
from skimage.feature import greycomatrix, greycoprops
from preprocessing import grayscale, resize
from skimage.util import view_as_windows
from numpy.lib.stride_tricks import as_strided
import matplotlib.pyplot as plt


# Receives a GLC matrix and returns 4 features: contrast, homogeneity, energy, entropy.
def calculate_haralick_features(glc_matrix, haralick_features):
    out_hf = []
    for haralick_feature in haralick_features:
        out_hf.append(greycoprops(glc_matrix, haralick_feature).sum(axis=1) / 4)
    return out_hf


# Receives a window (2d numpy matrix) and return a GLC matrix for given distance. The glc matrix is a collection
# of matrices for 0, 45, 90, and 135 degrees.
def calculate_glc_matrix(bytemap, distances=(1, 2, 3, 4, 5, 6), angles=(0, 45, 90, 135), levels=16):
    return greycomatrix(bytemap, distances=distances, angles=angles, levels=levels, symmetric=False)


# Placeholder metoda koja ja zamišljena da vraća sva okna za danu sliku
# Ovo je likely subject to change jer Bog zna šta ćemo morati sve znati o tim oknima down the road
# super_window je slika koju dijelimo na okna, a window_size je d
# Osim view_as_windows može i view_as_blocks ili as_strided ako ne ide nikako drugačije
def get_windows(super_window, window_size, step_size=None):
    if step_size is None:
        if window_size > 1:
            step_size = window_size // 2
        else:
            step_size = 1
    window_shape = (window_size, window_size)

    new_windows = view_as_windows(super_window, window_shape=window_shape, step=step_size)
    number_of_windows = new_windows.shape[0] * new_windows.shape[1]
    new_windows = new_windows.reshape(number_of_windows, window_size, window_size)
    return new_windows


def get_first_window(super_window, window_size):
    return super_window[0:window_size, 0:window_size]


# For testing purposes only
if __name__ == '__main__':
    distance = 12
    img_size = 4
    window_size = 2
    img_path = "./test_images/sample_crowd.png"
    img = Image.open(img_path)
    grayscale_img = np.array(
        grayscale(resize(img, img_size)),
        dtype=np.uint8)  # throws "ValueError: buffer source array is read-only" unless converted (?)
    print(grayscale_img)
    windows = get_windows(grayscale_img, window_size, step_size=window_size)
    print(windows)
    print(windows.shape)

    # glcm = calculate_glc_matrix(grayscale_img)
    # contrast, energy, homogeneity = calculate_haralick_features(glcm)
    # plt.subplot(211)
    # plt.title('Grayscale image')
    # plt.imshow(grayscale_img, cmap='gray')
    # plt.subplot(212)
    # plt.title(fr'GLCM mean')
    # plt.imshow(glcm.sum(axis=3)[:, :, 0], cmap='gray')
    # plt.show()
    # print(contrast, energy, homogeneity)
