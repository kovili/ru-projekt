from PIL import Image, ImageSequence
import numpy as np
import matplotlib.pyplot as plt


#Resize picture so that width and height are the same and equal to dimension. If show_picture is true, display the picture
def resize(imageSource: Image.Image, dimension=256, show_picture=False):
	"""
	Stvara novi objekt koji predstavlja sliku predanu preko parametra imageSource
	te ju smanjuje na dimenzije predane preko parametra dimensions.\n
	Za smanjivanje dimenzije se koristi bicubic algoritam.\n
	:param imageSource: slika koju se smanjuje
	:param dimension: dimenzije na koje ce se slika smanjiti
	:param show_picture: boolean koja odreduje hoce li se slika prikazati pri izvodenju
	:return: Slika promijenjenih dimenzija tipa PIL.Image.Image
	"""
	resized_image = imageSource.resize((dimension, dimension))

	if show_picture:
		plt.imshow(np.asarray(resized_image), 'gray')
		plt.show()

	return resized_image


# F in the chat za to_byte_map, you will be missed
# grayscale preuzima ulogu


#Gets RGB values for a picture and return grayscale. If show_picture is true, display the picture.
def grayscale(PILImage: Image.Image, levels=16, show_picture=False):
	"""
	Pretvaranje u grayscale sliku se provodi PIL.Image.convert() a pretvaranje u array je implementirano pomocu numpy-a.\n
	:param PILImage: Slika tipa PIL.Image.Image
	:param show_picture: boolean koji odreduje hoce li se prikazati slika ili ne
	:return: 2D array s grayscale vrijednostima istih dimenzija kao i predana slika.
	"""

	# L je mode gdje je svaki pixel reprezentiran jednim 8 bitnim brojem
	grayscale_image = np.array(np.asarray(PILImage.convert('L')) // (256 / levels), dtype=np.uint8)

	if show_picture:
		plt.imshow(grayscale_image, 'gray')
		plt.show()

	return grayscale_image


def get_frames_list(PILImage: Image.Image):
	"""
	Predana slika tipa PIL.Image.Image pretvara se u iterator koji se onda pretvara u listu
	:param PILImage:
	:return: Listu u kojoj je svaki objekt jedan frame iz sekvence slika
	"""
	return list(ImageSequence.Iterator(PILImage))
