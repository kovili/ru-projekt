import json

class ConfRunner:
    def __init__(self):
        self.configuration = dict()

    def parse_configuration_file(self, configuration_filename):
        with open(configuration_filename) as conf:
            self.configuration = json.load(conf)
        self.configuration["window_count"] = (self.configuration["resize_dim"] // (self.configuration["window_size"] // 2)) - 1
        self.configuration["vector_size"] = len(self.configuration["distances"]) * len(self.configuration["haralick_features"])

    def write_params(self, file):
        with open(file, 'w', encoding='utf-8') as conf_file:
            json.dump(self.configuration, conf_file, ensure_ascii=False, indent=4)

    def add_param(self, key, val):
        self.configuration[key] = val

    def get_param(self, key):
        return self.configuration[key]


if __name__ == '__main__':
    runner = ConfRunner()
    runner.add_param("distances", (1, 2, 3, 4, 5, 6))
    runner.add_param("angles", (0, 45, 90, 135))
    runner.add_param("resize_dim", 256)
    runner.add_param("window_size", 16)
    runner.add_param("grey_levels", 16)
    runner.add_param("haralick_features", ["contrast", "correlation", "energy", "homogeneity"])
    runner.add_param("train_folder", "./test_images")
    runner.add_param("test_folder_positive", None)
    runner.add_param("test_folder_negative", None)
    runner.add_param("database_path", None)
    runner.write_params("config")
    tester = ConfRunner()
    tester.parse_configuration_file("config")
    print(tester.configuration)
