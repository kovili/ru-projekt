 ### TODO ###
 ### Za većinu ovih zadataka će tijekom implementiranja biti potrebno hardkodirati neku sliku/glcm/okno, tj. potreban je dummy parametar s kojim će se testirati implementacija :) ### 
 ### Zapisati svoje ime na mjestu "NITKO" kada se preuzme zadatak tako da dvoje ljudi ne rade isti zadatak odjednom. Pushati na git. ###
 ### Staviti X ispred zadatka, ako je riješen ###
 
Kostur ------------- FD i PK
 
Preprocessing:	- Rijeseno za sad
	X 	Implementirati metodu resize ------------- Tarik
	X	Implementirati metodu to_byte_array ------------- Tarik
	X	Implementirati metodu grayscale ------------- Tarik
	X	Proučiti kako pomoću PIL-a dobiti frameove iz videa ------------- Tarik
	radi za gif, ne za video
	X   Kopirati/napisati isječak koda koji to radi za sample video.
	Napraviti kakav-takav main koji uzima putanju do datoteke sa slikama i iterira po svakoj slici i izvlači značajke iz njih pomoću implementiranih metoda. ------------- FD
	
Feature extraction:
	X  Implementirati metodu calculate_haralick_features ------------- Tesa
	X  Implementirati metodu calculate_glc_matrix ------------- Tesa
	X  Ako je moguće pomoću metode numpy.lib.stride_tricks.as_strided napraviti klizno okno. ------------- Tesa
	Metoda get_windows(image_bytemap, dimension) treba vratiti array matrica okana veličine d (dimension je argument f-je). Klizno okno se moze pomicati za t=d/2 desno ili dolje.
	Npr. ako je slika veličine (3*d,5*d) piksela, metoda treba vratititi 9 (po retku) * 5 (po stupcu) = 45 okana

Baza podataka:
	PRONAĐEN IZVOR. http://www.crowd-counting.com/#download

Testirati (opcionalno, ali poželjno stvoriti testne slučajeve gdje se vidi rad pojedine metode.):
	Implementirati metodu resize ---------- Karlo
	Implementirati metodu to_byte_array ---------- Karlo
	Implementirati metodu grayscale ---------- Karlo
	Proučiti kako pomoću PIL-a dobiti frameove iz videa
	Kopirati/napisati isječak koda koji to radi za sample video
	Implementirati metodu calculate_haralick_features
	Implementirati metodu calculate_glc_matrix
	

AJMO DALJE:
	X Z-score normalizacija ------------- Tesa
	X Multithreading u pythonu ------------- Tesa
	X Standardizirati rad s programom (conf datoteka?) ------------- Petar
	X Oznacavanje dijelova originalne slike na kojemu je mnostvo (ulaz ce vjv biti lista [(0|1)+] i broj prozora po retku/stupcu (jednaki su pa nmvz koji) ------------- Tarik
	- Izrada baze slika na kojima je mnoštvo (al onak puno od kraja do kraja) za učenje ------------- Petar, Karlo (moze vise ljudi) ---- Imamo pocetne slike za train
	- Izrada baze slika na kojima nema mnoštva (oko 100 slika za taj razred - za evaluaciju) ------------- Petar
	- Izrada baze slika na kojima je mnoštvo (za evaluaciju) ------------- Petar
